package es.upm.dit.apsv.traceconsumer2;

import java.util.Optional;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import es.upm.dit.apsv.traceconsumer2.model.Trace;
import es.upm.dit.apsv.traceconsumer2.model.TransportationOrder;
import es.upm.dit.apsv.traceconsumer2.repository.TraceRepository;
import es.upm.dit.apsv.traceconsumer2.repository.TransportationOrderRepository;

@SpringBootApplication
public class Traceconsumer2Application {

	public static final Logger log = LoggerFactory.getLogger(Traceconsumer2Application.class);

	@Autowired
	private TraceRepository traceRepository;

	@Autowired
	private TransportationOrderRepository transportationOrderRepository;

	public static void main(String[] args) {
		SpringApplication.run(Traceconsumer2Application.class, args);
	}

	// @Bean("consumer")
	// public Consumer<Trace> checkTrace() {
	// return t -> {
	// t.setTraceId(t.getTruck() + t.getLastSeen());
	// traceRepository.save(t);
	// String uri = env.getProperty("orders.server");
	// RestTemplate restTemplate = new RestTemplate();
	// TransportationOrder result = null;
	// try {
	// result = restTemplate.getForObject(uri + t.getTruck(),
	// TransportationOrder.class);
	// } catch (HttpClientErrorException.NotFound ex) {
	// result = null;
	// }

	// if (result != null && result.getSt() == 0) {
	// result.setLastDate(t.getLastSeen());
	// result.setLastLat(t.getLat());
	// result.setLastLong(t.getLng());
	// if (result.distanceToDestination() < 10)
	// result.setSt(1);
	// restTemplate.put(uri, result, TransportationOrder.class);
	// log.info("Order updated: " + result);
	// }
	// };
	// }

	@Bean("consumer")
	public Consumer<Trace> checkTrace() {
		return t -> {
			t.setTraceId(t.getTruck() + t.getLastSeen());
			traceRepository.save(t);
			Optional<TransportationOrder> result = transportationOrderRepository.findById(t.getTruck());
			TransportationOrder order = null;
			if (result.isPresent())
				order = result.get();

			if (order != null && order.getSt() == 0) {
				order.setLastDate(t.getLastSeen());
				order.setLastLat(t.getLat());
				order.setLastLong(t.getLng());
				if (order.distanceToDestination() < 10)
					order.setSt(1);
				transportationOrderRepository.save(order);
				log.info("Order updated: " + order.toString());
			}
		};
	}

}
